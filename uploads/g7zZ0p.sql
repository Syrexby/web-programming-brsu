-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 14 2013 г., 08:03
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ftp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `access_lvls`
--

CREATE TABLE IF NOT EXISTS `access_lvls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `access_lvls`
--

INSERT INTO `access_lvls` (`id`, `title`) VALUES
(1, 'admin'),
(2, 'Public');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `shortlink` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`shortlink`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `shortlink`) VALUES
(1, 'Category 1', '');

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(100) NOT NULL,
  `hashname` varchar(100) DEFAULT NULL,
  `access_lvl_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `last_mod_date` datetime NOT NULL,
  `size` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`,`hashname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `filename`, `hashname`, `access_lvl_id`, `category_id`, `last_mod_date`, `size`) VALUES
(1, '1.txt', '', 1, NULL, '2013-10-28 17:30:16', 5),
(2, '2.txt', '', 1, NULL, '2013-10-28 17:30:16', 5),
(3, '5elem.gif', 'HxjV0R.gif', 1, 1, '2013-12-14 00:45:19', 6607),
(4, '5elem.gif', 'D8MQuR.gif', 1, 1, '2013-12-14 02:02:43', 6607),
(5, 'atlant.jpg', '8TevGN.jpg', 2, 1, '2013-12-14 02:03:21', 34307),
(6, 'Dilis_logo.png', 'au6Uuf.png', 2, 1, '2013-12-14 02:06:21', 24886),
(7, 'fizz.jpg', 'lW11Xh.jpg', 2, 1, '2013-12-14 02:09:27', 18082),
(8, 'c6t2ocmvGSI.jpg', 'C2EExh.jpg', 1, 1, '2013-12-14 02:11:35', 64320),
(9, 'Zadacha_studenta_sdat_sessiyu_ne_ucha.jpg', 'Vcxte3.jpg', 1, 1, '2013-12-14 02:13:21', 52647),
(10, 'irina_i_mikhail_krug_-_prikhodite_v_moy_dom_(zaycev.net).mp3', '1cNy9S.mp3', 1, 1, '2013-12-14 02:19:16', 3555883),
(11, 'Kerry Force feat.M()eSTRo - Забавно - LD produсtion.mp4', 'U3a11m.mp4', 2, 1, '2013-12-14 02:21:04', 101582703),
(12, 'iTunes64Setup.exe', 'qcDNMj.exe', 1, 1, '2013-12-14 05:00:39', 90889040);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(25) NOT NULL,
  `email_reg` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(3) NOT NULL,
  `last_act` int(11) NOT NULL,
  `reg_date` int(11) NOT NULL,
  `access_lvl` varchar(100) NOT NULL,
  `online` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`user_login`),
  KEY `username_2` (`user_login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `user_login`, `email_reg`, `email`, `password`, `salt`, `last_act`, `reg_date`, `access_lvl`, `online`) VALUES
(1, 'admin', 'asdasdasd@ya.ru', 'asdasdasd@ya.ru', '0ef7ff3780e74b92254d5e0bb4d43354', '837', 1386993794, 1386988124, '', 1386993794),
(2, 'user1', 'asdasdasd@ya.ru', 'asdasdasd@ya.ru', '4376484ba2b319e4c783a0a28588fe80', '755', 1386993812, 1386990626, '', 1386993812);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
